<?php


namespace Edge\EdgeResolver\Computed;


use App\Http\Resources\ExportedFileResource;

class ExportedFile extends ComputedBase
{

    protected $resourceClass = ExportedFileResource::class;

    public $path = "";

    /**
     * SimpleBoolean constructor.
     * @param bool bool $result
     * @return ExportedFile
     */
    public function __construct($path, $unwrapped=true)
    {
        $this->path = $path;

        return $this;
    }

}