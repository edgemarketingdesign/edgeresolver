<?php


namespace Edge\EdgeResolver\Computed;


use Illuminate\Http\Resources\Json\JsonResource;
use Symfony\Component\ErrorHandler\Error\ClassNotFoundError;

class ComputedBase
{

    protected $resourceClass = "";

    protected $exception = null;


    /**
     * @return JsonResource|mixed
     * @throws \ReflectionException
     */
    public function exportResource()
    {
        $obj = $this->getReadyForResource();
        if ($this->resourceClass && class_exists($this->resourceClass)) {
            return new $this->resourceClass($obj);
        }

        $assumed = "App\\Http\\Resources\\" . (new \ReflectionClass($this))->getShortName() . "Resource";

        return new $assumed($obj);
    }

    /**
     * @return string
     */
    function getResourceClassName()
    {
        return $this->resourceClass;
    }

    /**
     * @return \Exception|null
     */
    function getException()
    {
        return $this->exception;
    }


    /**
     * @return ComputedBase
     */
    protected function getReadyForResource()
    {
        return $this;
    }


}