<?php


namespace Edge\EdgeResolver;

use Edge\EdgeResolver\Traits\HasOptions;
use Illuminate\Support\Facades\Storage;
use PDF;

class PrintDoc
{

    use HasOptions;

    protected $storageOptionKeys = [
        'saveFileName',
        'saveDisk',
        'saveFolder',
    ];

    protected $pdfOptionKeys = [
        'orientation',
        'encoding',
        'title',
        'header-right',
        'header-center',
        'header-line',
        'margin-left',
        'margin-right',
        'margin-top',
        'margin-bottom',
        'header-spacing',
    ];

    protected $contentKeys = [
        'columns',
        'rows',
        'title',
        'directives',
        'isPrint'
    ];

    protected $options = [
        "saveFileName" => "{random}.pdf",
        "saveDisk" => "s3",
        "saveFolder" => "uploads" . DIRECTORY_SEPARATOR . "documentCenter",
        'printView' => "print.export",
        'orientation' => 'portrait',
        'encoding' => 'UTF-8',
        'title' => "PdfExport",
        'isPrint'=>true,
        'exportedModel'=> "Edge\\EdgeResolver\\ExportedFile",
/*       'header-right' => 'Page [page]',
        'header-center' => "PdfExport",
        'header-line' => true,
        'margin-left' => 10,
        'margin-right' => 10,
        'margin-top' => 20,
        'margin-bottom' => 0,
        'header-spacing' => 8,*/
        'columns'=>[],
        'rows'=>[],
    ];

    public function print()
    {

        $columnsAndRows = $this->getOptions($this->contentKeys);
        ksort($columnsAndRows);

        $printView = \replaceParams($this->getOption("printView"));
        $filename = \replaceParams($this->getOption("saveFileName"));
        $folder = \replaceParams($this->getOption("saveFolder"));
        $disk = \replaceParams($this->getOption("saveDisk"));

        $path = $folder.DIRECTORY_SEPARATOR . $filename;

        $options = array_map(function($item){
            return \replaceParamsInFilePath($item);
        }, $this->getOptions($this->pdfOptionKeys));

        //$output = \Illuminate\Support\Facades\View::make($printView, $columnsAndRows)->render();

        /*        return response()->json(
            [$options,$path,$printView,$disk,$columnsAndRows]
        );*/

        /*$pdf = PDF::loadHTML($output);*/

        $pdf = PDF::loadView($printView, $columnsAndRows);
        $pdf->setOptions($options);


        Storage::disk($disk)->put($path, $pdf->output(), 'public');

        return Storage::disk($disk)->url($path);
    }

    public function getResponse(){

        /*return response()->json(["path"=>$this->print()]);*/


        $print = $this->print();

        $exportedClassName = trim(strval($this->getOption('exportedModel')));
        $exportedClassName = $exportedClassName ?: \Edge\EdgeResolver\Computed\ExportedFile::class;

        $model = new $exportedClassName($print);
        return $model->exportResource();
    }


}
