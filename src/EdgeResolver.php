<?php

/**
 * Flow:
 * 1 - Construct the class :) and read the global(present-free) settings
 * 2 - Initialize the class:
 *      Have the base builder(optional),
 *      Resolve Presets (resolves down rules, setting, defaultDirectives),
 *      Resolve Request (resolves down requestObject, requestDirectives)
 * 3 - Have the resource ready (resolves down resource):
 *      Fetch or set the resource
 *      * Sort Resource
 *      * Paginate resource
 *      Generate resource/print/export/apiresource
 * 4 - Return a result:
 *      Get Result (Override the return type if needed)
 * * - or run Cascaded: run actions by order without parameters
 */

namespace Edge\EdgeResolver;

use Edge\EdgeResolver\Computed\ExportedFile;
use Edge\EdgeResolver\Helpers\CollectionHelper;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Spatie\QueryBuilder\QueryBuilder;

class EdgeResolver
{

    // region Properties

    protected $configName = "edgeresolver";

    protected $globalSettings = [];

    protected $rules = []; // resolved rules from preset of the config
    protected $settings = []; // resolved settings from preset of the config
    protected $defaultDirectives = []; // resolved directives from preset of the config
    protected $requestDirectives = []; // resolved directives from the request
    protected $baseBuilder = null; // Assigned to a query obj from the init function or model class name
    protected $requestObject = null; // Illuminate\Http\Request instance

    protected $resource = null; // fetched or set resource - the main guy

    // extensive product from the resource. A (possibly sorted/paginated) collection, api response, etc.
    // At least, should be mirroring the resource
    protected $generatedObject = null;
    // endregion


    //region Phase 1: Initializing & Resolving Rules and Directives

    /**
     * Constructor
     *
     * @param mixed|null $preset Rules & def directives
     * @param \Illuminate\Http\Request|null $requestObj Request object
     * @param mixed|null $baseBuilder Base builder
     *
     * @return self
     */
    public function __construct(
        $preset = [],
        Request $requestObj = null,
        $baseBuilder = null,
        $configName = null
    ) {
        $this->setConfigName($configName);
        $fullConfigContent = \config($this->configName);
        $this->globalSettings = Arr::except($fullConfigContent, ["presets"]);
        return $this->init($preset, $requestObj, $baseBuilder);
    }

    public static function factory(
        $preset = [],
        Request $requestObj = null,
        $baseBuilder = null,
        $configName = null
    ):self {
        return new self($preset, $requestObj, $baseBuilder, $configName);
    }


    /**
     * Prepare the class
     *
     * @param mixed|null $preset Rules & def directives
     * @param \Illuminate\Http\Request|null $requestObj Request object
     * @param mixed|null $baseBuilder Base builder
     *
     * @return self
     */
    public function init(
        $preset = [],
        Request $requestObj = null,
        $baseBuilder = null
    ):self {


        $this
            ->resolvePreset($preset)
            ->resolveRequest($requestObj);

        $baseBuilder = $baseBuilder ?: $this->getSetting('modelClassName');

        if ($baseBuilder) {
            $this->setBaseBuilder($baseBuilder);
        }

        return $this;
    }


    /**
     * Map request input into a directives array
     *
     * @param Request $request Request object to map into the array
     *
     * @return self
     */
    public function resolveRequest(Request $request = null):self
    {
        $request = $request ?: \request();

        $this->requestObject = $request;

        $this->requestDirectives = $request->all();

        return $this;
    }

    /**
     * Retrieve the defaults from config or parameter
     *
     * @param array|null $defaultDirectives Directives array or config string
     *
     * @return self
     */
    public function resolveDefaults($defaultDirectives = null):self
    {
        if (is_array($defaultDirectives)) {
            $this->defaultDirectives = $defaultDirectives;
        } elseif (is_string($defaultDirectives)) {
            $readConfig = \config($defaultDirectives, []);
            $this->defaultDirectives = isset($readConfig["directives"]) ?
                $readConfig["directives"] :
                [];
        } else {
            $this->defaultDirectives = [];
        }

        return $this;
    }

    /**
     * Retrieve the rules from config or parameter
     *
     * @param array|null $rules Rules array or config string
     *
     * @return self
     */
    public function resolveRules($rules = null):self
    {
        if (is_array($rules)) {
            $this->rules = $rules;
        } elseif (is_string($rules)) {
            $readConfig = \config($rules, []);
            $this->rules = isset($readConfig["rules"]) ? $readConfig["rules"] : [];
        } else {
            $this->rules = [];
        }

        return $this;
    }

    /**
     * Retrieve the settings from config or parameter
     *
     * @param array|null $settings Settings array or config string
     *
     * @return self
     */
    public function resolveSettings($settings = null):self
    {
        if (is_array($settings)) {
            $this->settings = $settings;
        } elseif (is_string($settings)) {
            $readConfig = \config($settings, []);
            $this->settings = isset($readConfig["settings"]) ? $readConfig["settings"] : [];
        } else {
            $this->settings = [];
        }

        return $this;
    }


    /**
     * Retrieve the preset from config or array
     *
     * @param array|null $preset Preset array to be resolved
     *
     * @return self
     */
    public function resolvePreset($preset = null):self
    {

        $presetArray = [];

        if (is_array($preset)) {
            $presetArray = $preset;
        } elseif (is_string($preset)) {
            $presetArray = \config($preset, []);
        }


        $this->rules = isset($presetArray["rules"]) ? $presetArray["rules"] : [];
        $this->settings = isset($presetArray["settings"]) ? $presetArray["settings"] : [];
        $this->defaultDirectives = $presetArray["directives"] ?? [];

        if (!$this->baseBuilder && $setting = $this->getSetting('modelClassName')) {
            $this->setBaseBuilder($setting);
        }

        return $this;
    }


    /**
     * Lets the class detect the config key for default directives and retrieve it
     *
     * @return self
     */
    public function resolvePresetByRoute():self
    {
        $routeKey = Str::snake(
            str_replace('.', '_', \request()->route()->getName())
        );

        $routeKey = implode('.', array_filter([$this->configName, "presets", $routeKey]));
        $readConfig = \config($routeKey, null);

        if ($readConfig) {
            $this->resolvePreset($readConfig);
        }


        return $this;
    }

    /**
     * Grouped resolving function
     *
     * @param array|null $preset Directives to use as default
     * @param Request $requestObject Request object to map into the array
     *
     * @return self
     */
    public function resolveAll($preset = null, Request $requestObject = null):self
    {
        if ($preset) {
            $this->resolvePreset($preset);
        } else {
            $this->resolvePresetByRoute();
        }

        $this->resolveRequest($requestObject);

        return $this;
    }

    /**
     * Set the base builder
     *
     * @param mixed $builder The builder object
     *
     * @return self
     */
    public function setBaseBuilder($builder):self
    {
        $this->baseBuilder = QueryBuilder::for($builder);
        $modelClassName = $this->getSetting('modelClassName');
        if($modelClassName && defined($modelClassName."::allowedIncludes")){
            $this->baseBuilder->allowedIncludes($modelClassName::allowedIncludes);
        }

        return $this;
    }

    /**
     * Set the directives set to be used
     *
     * @param string $directivesSetName Name of the set to be used
     *
     * @return self
     */
    public function useDirectivesSet(string $directivesSetName):self
    {
        if (in_array($directivesSetName, ["default", "request", "merged"])) {
            $this->rules["useDirectivesSet"] = $directivesSetName;
        }

        return $this;
    }

    /**
     * Set the default directives to be used
     *
     * @return self
     */
    public function useDefaultDirectives():self
    {
        $this->rules["useDirectivesSet"] = "default";

        return $this;
    }

    /**
     * Set the Request directives to be used
     *
     * @return self
     */
    public function useRequestDirectives():self
    {
        $this->rules["useDirectivesSet"] = "request";

        return $this;
    }

    /**
     * Set the Merged directives to be used
     *
     * @return self
     */
    public function useMergedDirectives():self
    {
        $this->rules["useDirectivesSet"] = "merged";

        return $this;
    }

    //endregion


    //region Phase 2: Getting the resource ready

    /**
     * Fetches the resource after processing the builder
     *
     * @return self
     */
    public function fetchResource($sortAfterFetch = true):self
    {

        $builder = $this->baseBuilder;

        $this->applyRequestFilters($builder);

        $resourceType = $this->getSetting('resourceType', 'collection');

        switch ($resourceType) {
            case 'model':
                $method = $this->getDefaultDirective('resource.fetchMethod', 'first');
                break;
            case 'collection':
                $method = $this->getDefaultDirective('resource.fetchMethod', 'get');
                break;
            default:
                $method = $this->getDefaultDirective('resource.fetchMethod', 'get');
        }

        $this->resource = $builder->$method();

        //return dd($this->resource);


        if ($sortAfterFetch) {
            $this->sortResource();
        }

        return $this;
    }

    /**
     * Skip the fetching and inject the fetched
     *
     * @return self
     */
    public function setResource($resource):self
    {
        $this->resource = $resource;

        return $this;
    }

    /**
     * Sort the resource
     *
     * @param string|null $overrideField Override the field and use this
     * @param string|null $overrideDirection Override the dir. and use this
     *
     * @return self
     */
    public function sortResource(
        string $overrideField = null,
        string $overrideDirection = null
    ):self {

        $validTypes = ['collection', 'computed_collection'];

        if (
            $this->getRule('sorts', true) === false
            || !$this->resource
            || !in_array($this->getSetting("resourceType"), $validTypes)
        ) {
            return $this;
        }

        // convert direction to method
        $overrideMethod = null;
        if ($overrideDirection) {
            $overrideMethod = $overrideDirection == 'desc' ? 'sortByDesc' : 'sortBy';
        }

        // sync the defaults to overriding values (if even they are null)
        $method = $overrideMethod;
        $field = $overrideField;

        $directiveSet = $this->getRule("useDirectivesSet");

        /*
         * fallback to request directives for sorting
         * if either method or field is not overridden
         */

        if (
            !($method && $field)
            && in_array($directiveSet, ["request", "merged"])
            && $this->getRule("requestCanSort", true) !== false
        ) {
            $sort = $this->getRequestDirective('sort', '');
            $desc = Str::contains($sort, '-');

            if (!$method) {
                $method = $desc ? "sortByDesc" : "sortBy";
            }
            if (!$field) {
                $field = $desc ? ltrim($sort, '-') : $sort;
            }
        }

        /*
         * fallback to default directives for sorting
         * if either method or field is not overridden
         */
        if (
            !($method && $field)
            && in_array($directiveSet, ["default", "merged"])
        ) {
            if ($this->getDefaultDirective('sort.direction', 'asc') == 'desc') {
                $defaultMethod = 'sortByDesc';
            } else {
                $defaultMethod = 'sortBy';
            }

            $defaultField = $this->getDefaultDirective('sort.field', 'id');

            $method = $method ?: $defaultMethod;
            $field = $field ?: $defaultField;
        }


        $this->resource = $this->resource->$method($field);

        return $this;
    }

    /**
     * Paginate the resource
     *
     * @param $overrideLimit Override the limit value
     *
     * @return self
     */
    public function paginateResource(int $overrideLimit = null):self
    {

        $validTypes = ['collection', 'computed_collection'];

        if (
            $this->getRule('paginates', true) === false
            || !$this->resource
            || !in_array($this->getSetting("resourceType"), $validTypes)
        ) {
            return $this;
        }

        $limit = $overrideLimit;

        $directiveSet = $this->getRule("useDirectivesSet");

        if (
            !$limit
            && in_array($directiveSet, ["request", "merged"])
            && $this->getRule("requestCanPaginate", true) !== false
        ) {
            $limit = $this->getRequestDirective('limit');
        }

        if (
            !$limit
            && in_array($directiveSet, ["default", "merged"])
        ) {
            $limit = $this->getDefaultDirective('paginate.limit');
        }

        $limit = (int)$limit ? (int)$limit : 30;

        $this->resource = CollectionHelper::paginate(
            $this->resource,
            count($this->resource),
            $limit
        );

        return $this;
    }

    //endregion


    //region Phase 3: Return Response

    /**
     * Resturn the result
     *
     * @param $forceReturnType Override the computed return type
     *
     * @return mixed
     */
    public function getResult($forceReturnType = null)
    {
        if ($forceReturnType) {
            $returns = $forceReturnType;
        } else {
            $returns = $this->getDefaultDirective('return', 'apiResource');

            $directiveSet = $this->getRule("useDirectivesSet");

            if (in_array($directiveSet, ["request", "merged"])) {
                if (
                    $this->getRequestDirective('print') === 'true'
                    && $this->getRule("requestCanPrint", true) !== false
                ) {
                    $returns = 'print';
                }

                if (
                    $this->getRequestDirective('export') === 'true'
                    && $this->getRule("requestCanExport", true) !== false
                ) {
                    $returns = 'export';
                }
            }
        }


        switch ($returns) {
            case 'resource':
                $toReturn = $this->resource;
                break;
            case 'apiResource':
                $toReturn = $this->generateApiResource();
                break;
            case 'print':
                $toReturn = $this->generatePrint();
                break;
            case 'export':
                $toReturn = $this->generateExport();
                break;
            default:
                $toReturn = $this->resource;
                break;
        }

        return $toReturn;
    }


    public function cascaded($cascadedName)
    {

        $actions = \data_get($this->globalSettings, "cascadedActionSets." . $cascadedName, []);
        $actions = is_array($actions) ? $actions : [];

        foreach ($actions as $callable => $params) {
            $toReturn = call_user_func_array(array($this, $callable), $params);
        }
        return $toReturn;
    }

    //endregion


    //region Inner functionality

    /**
     * Get rule value
     *
     * @param string $key Key of the rule
     * @param mixed $default Default value to return if the key is not set
     *
     * @return mixed
     */
    protected
    function getRule(string $key, $default = null)
    {
        return \data_get($this->rules, $key, $default);
    }

    /**
     * Get setting value
     *
     * @param string $key Key of the setting
     * @param mixed $default Default value to return if the key is not set
     *
     * @return mixed
     */
    protected
    function getSetting(string $key, $default = null)
    {
        return \data_get($this->settings, $key, $default);
    }

    /**
     * Get request directive value
     *
     * @param string $key Key of the directive
     * @param mixed $default Default value to return if the directive is not set
     *
     * @return mixed
     */
    protected
    function getRequestDirective(string $key, $default = null)
    {
        return \data_get($this->requestDirectives, $key, $default);
    }

    /**
     * Get default directive value
     *
     * @param string $key Key of the directive
     * @param mixed $default Default value to return if the directive is not set
     *
     * @return mixed
     */
    protected
    function getDefaultDirective(string $key, $default = null)
    {
        return \data_get($this->defaultDirectives, $key, $default);
    }


    protected
    function composeColumns(
        $requestColumns = [],
        $defaultColumns = [],
        $fixedColumns = [],
        $allowRequestColumns = [],
        $disallowRequestColumns = []
    ) {

        /*
         * if requestColumns are not limited, immediately return them
         * merging with the fixed
        */

        if (!count($allowRequestColumns) && !count($disallowRequestColumns)) {
            return count($requestColumns) ?
                array_unique(array_merge($fixedColumns, $requestColumns)) :
                $defaultColumns;
        }

        /*
         * If requestColumns has items, step down filtering them.
         * If still have items after filtering, pack them with the fixed and return.
         * If all are filtered out, return the defaultColumns only.
        */
        if (count($requestColumns)) {
            if (count($allowRequestColumns)) {
                $requestColumns = array_intersect(
                    $requestColumns,
                    $allowRequestColumns
                );
            } else {
                $requestColumns = array_diff(
                    $requestColumns,
                    $disallowRequestColumns
                );
            }
            return count($requestColumns) ?
                array_unique(array_merge($fixedColumns, $requestColumns)) :
                $defaultColumns;
        }

        return $defaultColumns;
    }


    /**
     * Apply request filters on the builder
     *
     * @param Builder $builder Builder object
     *
     * @return self
     */
    protected
    function applyRequestFilters(&$builder):self
    {

        if (
            in_array($this->getRule("useDirectivesSet"), ["request", "merged"])
            && $this->getRule("requestCanFilter", true) !== false
        ) {
            $requestFilters = isset($this->requestDirectives['dynamic_filters']) &&
                is_array($this->requestDirectives['dynamic_filters']) ?
                $this->requestObject->dynamic_filters :
                [];
            $builder->allowedFilters($requestFilters);
        }

        return $this;
    }

    public
    function generateResource($paginate = true)
    {
        if ($paginate) {
            $this->paginateResource();
        }

        return $this->generatedObject = $this->resource;
    }

    public
    function generateApiResource()
    {
        $apiResClsName = $this->getSetting('apiResourceClassName');
        $apiResClsName = $apiResClsName ?: $this->assumeResourceClassName();


        $validTypes = ['collection', 'computed_collection'];

        return $this->generatedObject = in_array($this->getSetting("resourceType"), $validTypes) ?
            $apiResClsName::collection($this->resource) :
            new $apiResClsName($this->resource);
    }

    public
    function generatePrint(/* $overrideLayout = null */)
    {

        $directiveSet = $this->getRule("useDirectivesSet");
        $requestCanModifyDocSettings = $this->getRule("requestCanModifyDocSettings", true);
        $filterSchemaOverridesDocTitle = $this->getRule("filterSchemaOverridesDocTitle", true);
        $filterSchemaTitle = null;
        $layout = 'default';
        $columns = [];

        if(in_array($directiveSet, ["request", "merged"])){
            $layout = $this->getRequestDirective('layout', 'default');
            $columns = $this->getRequestDirective('printedColumns', []);
        }
        $docDirectives = $this->getDefaultDirective("print.layouts.$layout");

        if (
            in_array($directiveSet, ["request", "merged"])
            && $filterSchemaOverridesDocTitle
        ) {
            $filterSchemaId = $this->getRequestDirective('filter_schema', null);
            $filterSchemaClassName = 'App\Models\FilterSchema';
            if (
                $filterSchemaId
                && class_exists($filterSchemaClassName)
                && $filterName = $filterSchemaClassName::whereId($filterSchemaId)->value('filter_name')
            ) {
                $docDirectives['title'] = $filterName;
            }
        }

        if (in_array($directiveSet, ["request", "merged"]) && $requestCanModifyDocSettings) {
            $docDirectives = array_merge($docDirectives, $this->getRequestDirective('docParams',[]));
        }

        $columns = $this->composeColumns(
            $columns,
            Arr::get($docDirectives, 'defaultColumns', []),
            Arr::get($docDirectives, 'fixedColumns', []),
            Arr::get($docDirectives, 'allowRequestColumns', []),
            Arr::get($docDirectives, 'disallowRequestColumns', []),
        );

        $doc = new PrintDoc($docDirectives);

        $optionsToSet = ["columns" => $columns, "rows" => $this->resource];

        $exportedFileClassName = \data_get($this->globalSettings, "exportedFileClassName", "");
        $exportedFileClassName = trim($exportedFileClassName) ? $exportedFileClassName : ExportedFile::class;

        $optionsToSet["exportedModel"] = $exportedFileClassName;
        $optionsToSet["directives"] = $docDirectives;

        $doc->setOptions($optionsToSet);

        return $this->generatedObject = $doc->getResponse();
    }

    public
    function generateExport()
    {

        $directiveSet = $this->getRule("useDirectivesSet");
        $requestCanModifyDocSettings = $this->getRule("requestCanModifyDocSettings", true);
        $filterSchemaOverridesDocTitle = $this->getRule("filterSchemaOverridesDocTitle", true);
        $filterSchemaTitle = null;
        $layout = 'default';
        $columns = [];

        if(in_array($directiveSet, ["request", "merged"])){
            $layout = $this->getRequestDirective('layout', 'default');
            $columns = $this->getRequestDirective('printedColumns', []);
        }

        $docDirectives = $this->getDefaultDirective("export.layouts.$layout");

        if (
            in_array($directiveSet, ["request", "merged"])
            && $filterSchemaOverridesDocTitle
        ) {
            $filterSchemaId = $this->getRequestDirective('filter_schema', null);
            $filterSchemaClassName = 'App\Models\FilterSchema';
            if (
                $filterSchemaId
                && $filterName = $filterSchemaClassName::whereId($filterSchemaId)->value('filter_name')
            ) {
                $docDirectives['title'] = $filterName;
            }
        }

        if (in_array($directiveSet, ["request", "merged"]) && $requestCanModifyDocSettings) {
            $docDirectives = array_merge($docDirectives, $this->getRequestDirective('docParams',[]));
        }

        $columns = $this->composeColumns(
            $columns,
            Arr::get($docDirectives, 'defaultColumns', []),
            Arr::get($docDirectives, 'fixedColumns', []),
            Arr::get($docDirectives, 'allowRequestColumns', []),
            Arr::get($docDirectives, 'disallowRequestColumns', []),
        );

        $doc = new ExportDoc($docDirectives);

        $optionsToSet = ["columns" => $columns, "rows" => $this->resource];

        $exportedFileClassName = \data_get($this->globalSettings, "exportedFileClassName", "");
        $exportedFileClassName = trim($exportedFileClassName) ? $exportedFileClassName : ExportedFile::class;

        $optionsToSet["exportedModel"] = $exportedFileClassName;
        $optionsToSet["directives"] = $docDirectives;

        $doc->setOptions($optionsToSet);

        return $this->generatedObject = $doc->getResponse();
    }

    protected
    function assumeResourceClassName()
    {
        return "App\\Http\\Resources\\" .
            substr(strrchr($this->getSetting('modelClassName'), "\\"), 1) .
            "Resource";
    }


    public function setConfigName($configName):self
    {
        if (trim(strval($configName))) {
            $this->configName = $configName;
        }
        return $this;
    }

    public function getConfigName(): string
    {
        return $this->configName;
    }


    //endregion

}
