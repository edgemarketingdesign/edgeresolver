<?php


namespace Edge\EdgeResolver;


use Edge\EdgeResolver\Traits\HasOptions;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class ExportDoc
{

    use HasOptions;

    protected $options = [
        "title"=>"XlsExport",
        "saveFileName" => "{random}.xlsx",
        "saveDisk" => "s3",
        "saveFolder" => "uploads" . DIRECTORY_SEPARATOR . "documentCenter",
        'exporterModel' => "App\\Exports\\TableExport",
        'exportedModel'=> "Edge\\EdgeResolver\\ExportedFile",
        'columns'=>[],
        'rows'=>[],
    ];

    public function export(){

        $filename = $this->getOption("saveFileName");
        $folderPath = $this->getOption("saveFolder");
        $disk = $this->getOption("saveDisk");

        $rows = $this->getOption("rows");
        $columns = $this->getOption("columns");
        $title = $this->getOption("title");

        $addColumns = $this->getOption("addColumns");
        $addColumns = is_array($addColumns) ? $addColumns : [];

        if(count($addColumns)){
            array_walk($addColumns,function($value)use(&$columns){
                $columns[]= $value;
            });
            $columns = array_unique(array_filter($columns));
        }


        $exporterClassName = $this->getOption("exporterModel");

        $fullFilePath = \replaceParamsInFilePath($folderPath . DIRECTORY_SEPARATOR . $filename);

        /*return response()->json([
            $exporterClassName,$fullFilePath
        ]);*/

        Excel::store(new $exporterClassName(["rows"=>$rows, "columns"=>$columns, "title"=>$title, "directives"=>$this->getOption("directives",[])]), $fullFilePath, $disk, null, [
            'visibility' => 'public',
        ]);

        return Storage::disk($disk)->url($fullFilePath);
    }

    public function getResponse(){

        $export = $this->export();


        $exportedClassName = trim(strval($this->getOption('exportedModel')));
        $exportedClassName = $exportedClassName ?: \Edge\EdgeResolver\Computed\ExportedFile::class;

        $model = new $exportedClassName($export);
        return $model->exportResource();

    }

}
