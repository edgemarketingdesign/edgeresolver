<?php

namespace Edge\EdgeResolver\Traits;


trait HasOptions{

    //protected $options=[];


    public function __construct($initialOptions = [])
    {
        $this->options($initialOptions);

    }

    public function option($key, $value = null)
    {
        return func_num_args() == 2 ? $this->setOption($key, $value) : $this->getOption($key);
    }

    public function options($options = null)
    {
        return $options ? $this->setOptions($options) : $this->getOptions();
    }

    public function setOption($key, $value)
    {
        $this->options[$key] = $value;
    }

    public function getOption($key, $default=null)
    {
        return isset($this->options[$key]) ? $this->options[$key] : $default;
    }

    public function setOptions($options)
    {
        foreach($options as $key=>$value){
            $this->setOption($key,$value);
        }
        //array_merge_recursive($options,$this->options);
    }

    public function getOptions(...$keys)
    {
        if(count($keys)){
            $keys = $this->flatten_array($keys);
            return $this->only_array($this->options, $keys);
        }
        return $this->options;
    }

    public function flatten_array($array, $depth = INF)
    {
        $result = [];

        foreach ($array as $item) {
            $item = $item instanceof \Illuminate\Support\Collection ? $item->all() : $item;

            if (! is_array($item)) {
                $result[] = $item;
            } else {
                $values = $depth === 1
                    ? array_values($item)
                    : $this->flatten_array($item, $depth - 1);

                foreach ($values as $value) {
                    $result[] = $value;
                }
            }
        }

        return $result;
    }

    public function only_array($array, $keys)
    {
        return array_intersect_key($array, array_flip((array) $keys));
    }


}
