<?php

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Spatie\QueryBuilder\AllowedFilter;


if (!function_exists("replaceParamsInFilePath")) {
    function replaceParamsInFilePath($text, $additionalSet = [])
    {
        $preset = [
            "{random}" => rand(1, 100000),
        ];
        $set = array_merge_recursive($preset, $additionalSet);

        return strtr($text, $set);
    }
}

if (!function_exists('formatCellData')) {
    function formatCellData($value, $type)
    {
        $formatted = $value;

        switch ($type){
            case 'date':
                $formatted = $value ? Carbon::parse($value)->format("m/d/Y") : $value;
                break;
            case 'time':
                $formatted = $value ? Carbon::parse($value)->format("H:i A") : $value;
                break;
            case 'datetime':
                $formatted = $value ? Carbon::parse($value)->format("m/d/Y H:i A") : $value;
                break;
            case 'money':
                $formatted = number_format($value/100,2,'.',',');
                break;
            case 'moneyRaw':                //== U.D dues where it is stored as float, I think
                $formatted = number_format($value,2,'.',',');
                break;
            case 'phone':
                $numbersOnly = preg_replace("/[^\d]/", "", $value);
                $formatted = preg_replace("/^1?(\d{3})(\d{3})(\d{4})$/", "($1) $2-$3", $numbersOnly);
                break;
            case 'checkbox':                //(boolean)
                $value = (bool) $value;
                $formatted = $value ? "Yes" : "No";
                break;
            case 'text':
                break;
            case 'textarea':
                break;
            case 'wysiwyg':
                $formatted = strip_tags($value);
                break;
            case 'email':
                break;
            case 'states':
                break;
            case 'website':
                break;
            case 'select':
                break;
        }

        return $formatted;

    }
}

