<?php

namespace Edge\EdgeResolver;

use Illuminate\Support\ServiceProvider;

class EdgeResolverServiceProvider extends ServiceProvider
{


    public function boot()
    {

        $this->loadViewsFrom(__DIR__ . '/../resources/views', 'EdgeResolver');

        $this->publishes([__DIR__ . '/../config'  => config_path()], "EdgeResolver");
        // $this->publishes([
        //     __DIR__ . '/resources/views' => $this->app->resourcePath('views/vendor/edgeresolver'),
        // ], 'edgeresolver');

        if(file_exists(__DIR__.'/../Http/'. 'helpers.php')){
            include_once __DIR__.'/../Http/'. 'helpers.php';
        }
    }

    public function register()
    {


    }
}
