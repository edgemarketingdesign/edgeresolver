<?php

return [
    "exportedFileClassName" => 'Edge\\EdgeResolver\\Computed\\ExportedFile',
    "cascadedActionSets" => [
        "quickIndex" => [
            "resolvePresetByRoute" => [],
            "fetchResource" => [],
            "paginateResource" => [],
            "getResult" => [],
        ],
        "quickShow" => [
            "resolvePresetByRoute" => [],
            "fetchResource" => [],
            "getResult" => [],
        ],
    ],
    "presets" => [
        "example_preset" => [
            "rules" => [
                "sorts" => true,
                "paginates" => true,
                "requestCanSort" => true,
                "requestCanFilter" => true,
                "requestCanPaginate" => true, //"requestCanInclude" => true (requires inordinary adjustments, will be developed when needed)
                "requestCanPrint" => true,
                "requestCanExport" => true,
                "requestCanReturnResource" => true,
                "requestCanReturnApiResource" => true,
                "requestCanModifyDocDirectives" => true,
                "filterSchemaOverridesDocTitle" => true,
                "useDirectivesSet" => "merged" // default, request, merged,
            ],
            "settings" => [
                'resourceType' => "collection",
                //types: model, collection, computed_model, computed_collection
                'modelClassName' => 'App\\Models\\NewModel',
                "apiResourceClassName" => "App\\Http\\Resources\\NewModelResource",
            ],
            "directives" => [
                "return" => "apiResource", // resource, apiResource, print, export
                "resource" => [
                    "sort" => [
                        "field" => "id",
                        "direction" => "asc"
                    ],
                    "paginate" => [
                        "limit" => 5
                    ]
                ],
                "print" => [
                    'layouts' => [
                        "default" => [
                            "title" => "New Report",
                            "saveDisk" => 'local',
                            'printView' => "print.pdf",
                            "saveFolder" => "uploads" . DIRECTORY_SEPARATOR . "{random}" . DIRECTORY_SEPARATOR . "prints",
                            "saveFileName" => "NewReport{random}.pdf",
                            "orientation" => "landscape",
                            "encoding" => "UTF-8",
                            "page-size" => "letter",
                            "defaultColumns" => [],
                            "fixedColumns" => [],
                            "allowRequestColumns" => [],
                            "disallowRequestColumns" => []
                        ]
                    ]
                ],
                // for exporting
                "export" => [
                    'layouts' => [
                        "default" => [
                            "title" => "New Report",
                            "exporterModel" => 'App\\Exports\\DefaultExport',
                            "saveDisk" => 'local',
                            "saveFolder" => "uploads" . DIRECTORY_SEPARATOR . "{random}" . DIRECTORY_SEPARATOR . "exports",
                            "saveFileName" => "NewReport{random}.xlsx",
                            "defaultColumns" => [],
                            "forceColumns" => [],
                            "disallowColumns" => []
                        ]
                    ]
                ],
            ]
        ]
    ]
];
